FUNCTIONS, SUBROUTINES

\----------------------

Continued from last week...

* Function: one answer value, like in algebra.
* Subroutine: no answer value, just a mini-program helper.
* Note: Java refers to functions and subroutines as "methods".


ISSUES:

* In a function, a "return" statement sets the answer value,
  but also exits the function.

* In a subroutine, "return;" exits the subroutine early.
  Also, for main routines.

* Using one huge main routine:
  Is inlined programming.  Is efficient, but complex to look at.

* Break main routine into functions, subroutines:
  Is modular programming.  Is inefficient, but easy to look at, change, etc.

* In Java, unlike some languages, you can list
  the main routine, functions, subroutines,
  in whichever sequence you prefer.


\---------------------------------------------------------------------------.


ARRAYS
======


import java.util.*;
public class Prog1
{
public static void main(String args[])
{


int[] a = new int[10];    //creates 10:  a[0] to a[9]

System.out.println( a.length );

a[0] = 10;
a[1] = 100 * a[0];

System.out.println( a[0] );
System.out.println( a[1] );

Arrays.sort(a);  //requires java.util

System.out.println(Arrays.toString(a));  //requires java.util


for (int i=0; i&lt;=9; i++)
{
   a[i] = i+1;
}

System.out.println(Arrays.toString(a));  //requires java.util

for (int i=0; i&lt;=9; i++)
{
   System.out.println( "a[" +  i  +  "]="  +  a[i] );
}



int[] x = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
     //creates 10 elements, x[0] to x[9], sets them = 1 to 10

for (int i=0; i&lt;=9; i++)
{
   System.out.println( x[i] );
}


for (int i=0; i&lt;x.length; i++)
{
   System.out.println( x[i] );
}


for (int t : x)    //FOR EACH loop
{
   System.out.println(t);
}



//normal variables:  you must declare and initialize prior to
//                    working with them

//array variables: you must declare.
//    if you initialize (eg, x), you get the first values.
//    if not (eg, a), you get all 0's automatically!
//                      eg, Java initializes arrays for you!


}
}


\---------------------------------------------------------------------------


SPECS:

1. Input a size from the user, assuming 1 or larger.

2. Create an array of that size.

3. Fill it with the even numbers,
   starting at 0, e.g.:   0, 2, 4, 6, 8, ...
   the even numbers throughout the entire array.


import java.util.*;
public class p1
{
public static void main(String args[])
{
Scanner scan = new Scanner(System.in);


System.out.println("Input an array size, 1 or larger:");
int n = scan.nextInt();


int[] a = new int[n];


for (int i=0; i&lt;=n-1; i++)   //OR: for (int i=0; i&lt;n; i++)
{                            //OR:  for (int i=0; i&lt;a.length; i++)
   a[i] = 2*i;
}
        //OR:   for (int i=0; i&lt;=2*(n-1); i=i+2)
        //      {
        //         a[i/2] = i;
        //      }

System.out.println(Arrays.toString(a));


System.out.println("The array is:");
for (int i=0; i&lt;=n-1; i++)   //shortcut: second slot is: i&lt;n
{                            //or also:  second slot is: i&lt;a.length
   System.out.println(a[i]);
}


System.out.println("Fast array output:");
System.out.println( Arrays.toString(a) );


System.out.println("Outputting array, using FOR EACH loop:");
for (int t : a)
{
   System.out.println(t);
}


} //main
} //class


\----------------------------------------------------------------------------


//FILE BROWSER
//
//Note: When compiling, ignore the "unchecked" message that might show up.
//      That's not an error.  It is just telling you that you can use
//      generics Stacks in the program, which is a topic discussed later.


import java.util.*;
import java.io.*;
public class p2
{
public static void main(String args[]) throws IOException
{
Scanner scan = new Scanner(System.in);


System.out.println("Input a filename pattern:");
String p = scan.nextLine();


Stack st1 = new Stack();
st1.push(".");


while ( ! st1.isEmpty() )
{

   String fullname = (String) st1.pop();
   File f1 = new File(fullname);

   if (f1.isFile())
   {
      if (fullname.contains(p))
      {
         System.out.println("***** FOUND FILE: " + fullname);
      }
   }
   else if (f1.isDirectory())
   {
      String[] tt = f1.list();

      for (int i=0; i&lt;tt.length; i++)
      {
         st1.push( fullname + "/" + tt[i] );
      }
   }

}


} //main
} //class


\----------------------------------------------------------------------------



