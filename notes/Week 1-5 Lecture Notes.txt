MACHINE LANGUAGE, ASSEMBLY LANGUAGE, HIGH-LEVEL LANGUAGES







Bit = "binary digit" = 0 or 1

Computers store information in bits.

Usually, bits are grouped into units of 8 (one "byte").




Byte = 8 bits

Kilobyte = 2^10 = 1,024 bytes

Megabyte = 2^20 = 1,048,576 bytes




Binary = base-2 number system = used by computers = digits are 0 and 1

Decimal = base-10 number system = used by people = digits are 0 to 9

Hexadecimal = base-16 number system =

            = easy way for programmers to write binary numbers =

            = digits are 0 to 9, and A to F

Examples:

decimal 19, written in binary, is 10011, because 1*(2^4) + 1*(2^1) + 1*(2^0)=19

decimal 19, written in decimal, is 19, because 1*(10^1) + 9*(10^0) = 19

decimal 19, written in hexadecimal, is 13, because 1*(16^1) + 3*(16^0) = 19







RAM = random-access memory.

It's possible to read data from, and write data to, RAM memory.

Therefore, your program, and any data your program uses or needs,

is loaded into your computer's RAM memory.  The more RAM memory your

computer has, the bigger your programs and data can be.




ROM = read-only memory.

You can read the data values, but you can't change them.

Usually, this is where important system-related programs and data are

stored, by the company that made your computer.




CPU = Central Processing Unit.

Examples: the Intel Pentium microprocessor for PCs, the Intel 486

microprocessor for PCs, the Sun Ultra SPARC processor for UNIX machines,

the G5 processor for Macs, etc.

The CPU usually looks like a black plastic square or rectangle, usually

about the size of a half-dollar, with short metal wires or connectors

coming out of its sides.  Inside this case is the CPU's electronics.




The CPU is the "brain" of a computer -- it controls everything,

does all the math, controls the reading/writing of data, executes the

instructions in your program, etc.










MACHINE LANGUAGE --

\-------------------

*. Each type of CPU is designed to only do certain things

   (such as "load a number from memory," "store a number into RAM memory,"

   "add two numbers," "multiply two numbers," etc.)

*. But since CPUs can only understand numbers (patterns of "bits"),

   the commands that a programmer gives to the CPU must be given

   in terms of numbers.  Therefore, a CPU's designers must pair

   a specific number code with each thing that the CPU can do.

*. Those specific numbers are usually called "Op Codes" (operations codes).

*. Since CPUs only understand op codes, computer programs must be written

   entirely in op codes.

*. Machine language is a "low-level" language -- this means that computers

   can understand it, but humans have a hard time understanding it.







ASSEMBLY LANGUAGE --

\--------------------

*. Assembly languages made programming easier for you, because the

   "machine language op codes" were replaced by short codewords that are

   easy for you to remember.

*. After you write a program in assembly language, you give your program

   to a special piece of software called an "assembler" -- this software

   reads your assembly-language program, and then creates a new file

   containing the machine-language version of your program.

   Since you now have a machine-language file, you can "run" it.







HIGH-LEVEL LANGUAGES --

\-----------------------

*. High-level languages are hard (impossible) for computers (CPUs) to

   understand, but are easy for humans to understand.  (Remember, "low-level"

   languages are easy for computers, but hard for humans.)

*. Well-known high-level languages are: BASIC, FORTRAN, C, C++, Java, etc.

*. High-level languages make programming extremely easy, because they

   are usually designed to resemble human language.




*. After you write a program in a high-level language, you give your program

   to a special piece of software called a "compiler" -- this software

   reads your high-level language program, and then creates a new file

   containing the machine-language version of your program.

   Since you now have a machine-language file, you can "run" it!




*. There is, however, one disadvantage.  A compiler might not always

   make the best/optimal choices during the conversion to machine language.

   This can result in machine language code that is very fast, but not as fast

   as it could have been if it were designed carefully by hand.

*  In programs that require the fastest possible speeds, programmers often

   use assembly language so they can manually choose

   their program's machine language very carefully,

   to make it as streamlined and fast as possible.







EXAMPLE --

\----------

Write a program that multiplies two numbers, adds a third number,

and then stores the result into a fourth variable.

In mathematics notation:  D = A*B + C




Assume that A's value is currently stored in RAM memory location 0000,

and B's value is stored in location 0001, and C's value is stored in 0002.

Also, assume that we'll use location 0003 to store D's computed value.







TASK           IN MACH. LANG.:     IN ASSEMBLY LANG:     IN A HI-LEVEL LANG:

\------------   ---------------     -----------------     -------------------




look at value         10             load 0000             D = A*B + C

stored in mem         00   

loc 0000, then load   00   

the value into CPU




mult it by            24             mult 0001

the value stored      00  

in mem loc 0001       01   




add value             23             add 0002

stored in             00  

mem loc 0002          02   




store result          11             store 0003

into mem              00    

loc 0003              03   




stop                  60             stop

(end program)







\-------------------------------------------------------------------------







WHY JAVA PROGRAMS CAN RUN ON "ANY" COMPUTER SYSTEM

\--------------------------------------------------




There are two basic types of programs:

"interpreted" programs, and "compiled" programs.




As explained above, programs written in compiled languages (FORTRAN, C,

C++, etc.) are converted into machine language by a compiler.  Since you have

a machine language program, the program runs at top speed.




Programs written in interpreted languages (UNIX shell scripts, JavaScript,

most dialects of BASIC, etc.) are read line by line, and each line is

converted to machine language and executed as soon as the line is read.

Interpreted programs are generally slow, because of the translation step

that must be done for each command, while the program is running.




Java is sort of a "half-compiled, half-interpreted" language.

(Explained below.)  This makes it faster than interpreted languages,

but generally slower than compiled languages.  Sun Microsystems invented Java,

and is always looking for ways to make each new Java version

faster than the last.  (Note: Sun Microsystems is now owned by Oracle.)







Does Java really run on "any" computer system?

\----------------------------------------------

Technically, it doesn't -- it only runs on computer systems for which

SunMicrosystems/Oracle (or other companies) created

"Java interpreter" software.







The Java Compiler, the Java Interpreter, the Java Virtual Machine

\-----------------------------------------------------------------

These are the secrets behind Java's cross-platform (computer-independent)

capability.




* When you compile a FORTRAN program on Rohan, you end up with a program

  written in the machine language of Rohan's CPU (e.g., Sun Ultra SPARC).




* When you compile a C++ program on your PC, you end up with a program

  written in the machine language of your PC's CPU (e.g., Intel Pentium).




* But when you compile a Java program (using the "javac" command), you end up

  with a program written in the machine language of an imaginary computer/CPU

  called the "Java Virtual Machine" (JVM).




* This machine language program is referred to as "Java bytecode", and it

  has a filename like MyProg.class




  (For FORTRAN, C++, etc., the machine language code is usually referred

  to as "object code" or "machine language code", and it usually has filenames

  like a.out or myprog.exe.  The machine language file is usually called

  the "executable file" because you can execute/run it.)




* You can then give your Java bytecode file to your friend.

  Your friend then feeds the Java bytecode file into

  the Java interpreter software installed on his/her computer system.

  (Your friend does this by using the command "java", which is the name

  of his/her Java interpreter software.)

  Your Java program will now run on your friend's computer system,

  no matter what kind of computer system it is.




* The Java interpreter works by "interpreting" the Java bytecode.

  That is, it reads your bytecode file's "JVM machine language", and then

  (like all interpreters) converts it into the current computer's/CPU's

  machine language and then executes that machine language,

  all while the program is currently running.




* This is why Java is sometimes said to be a "half-compiled and

  half-interpreted" language.




* What Java lacks in speed, it makes up in cross-platform capability.







\-----------------------------------------------------------------------------

\-----------------------------------------------------------------------------







STUDENT ACCOUNTS ON ROHAN




Student accounts (the username is usually your last name)

can be obtained online.

Log in to your WebPortal, and click the link "Get a ROHAN/Email account".




An account will be added for you.

The account information will be displayed on the screen,

and will be emailed to your email address on file with the WebPortal.







\-----------------------------------------------------------------------------







CONNECTING TO ROHAN

\-------------------




You can do your programming work on campus, or remotely,

by connecting to rohan.

An SSH connection is basically a "secure" connection to a remote computer.







OBTAINING SSH SOFTWARE, FOR WINDOWS COMPUTERS:

\----------------------------------------------




Windows users can obtain PuTTY at its official download link:




   http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe




But, if PuTTY doesn't work on your Windows computer, you can

try this software instead:




   ftp://rohan.sdsu.edu/pub/ibm/SSHSecureShellClient-3.2.9.exe







OBTAINING SSH SOFTWARE, FOR MAC COMPUTERS:

\------------------------------------------




SSH software is already installed on Mac OS X computers.










HOW TO SSH-CONNECT TO ROHAN WITH MAC OS X

\-----------------------------------------




1. Run your Mac's "Terminal" program.

   It's usually in the "Applications/Utilities" folder,

   but on some systems it might be in the "Accessories" folder.

   If you can't find it, then "search" your system for the "Terminal" program.

   When you run Terminal, it will look like a Unix window very similar

   to the rohan window that we see in lecture.




2. At your Mac's Unix prompt, type the following command, to connect to rohan:

   ssh username@rohan.sdsu.edu

   where username is replaced by the username of your rohan account.

   Rohan will ask you for your password, so just enter it.

   You'll then be logged in to your rohan account.




   Note: if rohan asks about your terminal type, type: vt100




   You should now be at rohan's "rohan%", or "bash$", or "&gt;", etc.,

   Unix prompt.  Therefore, you are ready to type Unix commands at the prompt,

   just like during lecture.




   When you are finished with your rohan session, always remember

   to type "logout" or "exit" to log out of your rohan account.




If you try to run vi or pico, but rohan gives you an error,

then go to your Terminal software's preferences menus, to change

your screen type from "xterm" to "vt100".

Close then re-start Terminal, to make your changes take effect.




The first time you connect to rohan with your Mac's SSH software,

SSH might require you to confirm your destination, by asking you whether you

want to accept a "key".  Just accept it.










HOW TO SSH-CONNECT TO ROHAN WITH PUTTY

\--------------------------------------




Note: when you start PuTTY on Windows XP, WinXP might pop up a

warning/confirmation before allowing it to run.  WinXP does this

for all un-signed executable files, so don't be surprised.




When your run PuTTY, you will see PuTTY's main control panel.




In the "Host Name (or IP Address)" textbox, type:




rohan.sdsu.edu




In the "Protocol" section, click to choose "SSH" (if it isn't

already selected).  The "Port" field should now automatically display "22".




In the "Saved Sessions" textbox, give your rohan connection

a name, such as "My Rohan", or whatever you want.




Now click the "Save" button.




You can now exit PuTTY, by clicking the usual "X" close-button

at the top-right corner of the window.




EACH TIME YOU RUN PUTTY:




Just double-click your profile name, "My Rohan", to connect to rohan.

(Extra Note: the first time you do this, PuTTY will ask you

to confirm your destination address.  Click "Yes" to confirm/allow rohan.)




After PuTTY has established the connection to rohan, rohan should respond

by asking you for your username and password.  Type that info when asked.




You are now logged on to your rohan account, and should

see the usual "rohan%", or "bash$", or "&gt;", or etc., rohan Unix prompt.

You are ready to do your work.




When you are finished with your rohan session, always remember

to type "logout" or "exit" to log out of your rohan account.

(Note: when you logout, PuTTY may or may not automatically close,

depending on your customized preferences/settings.)




CUSTOMIZING YOUR PUTTY PREFERENCES/SETTINGS




* Start PuTTY, then single-click to select your "My Rohan" profile name,

  then click the "Load" button.  This will load your rohan profile.




* To adjust the fonts and colors of your SSH window,

  you can click the "Appearance" and "Colours" sub-items.

  To change the text color, set the "Colours" sub-item's

  "Default Foreground" property.  To set the background color, set the

  "Colours" sub-item's "Default Background" property.

  To change the font or font size, use the "Appearances" sub-item.




* After choosing all the desired values for Keyboard, Appearance, Colours,

  etc., you are now ready to save your changes.  First, click the

  "Session" item in the "tree" structure on the left side of the

  control panel.  This will display PuTTY's initial, start-up screen.

  Then, click the "Save" button to save all your new preferences.

  You can now click the "X" in the upper-right corner of the window,

  to close your PuTTY software.  The next time you connect to rohan,

  your desired backspace, font, color, etc. preferences, will be in effect.







\-----------------------------------------------------------------------







INTRO TO UNIX

\-------------




Unix is "case-sensitive", so type commands with the proper case.




File names and directory names can be anything,

so you can use whatever capitalization you want.




Beginning students often try to put "spaces" in their file names or

directory names.  Do not do that!  Although there's a way for advanced users

to insert "spaces" in their file and directory names, beginners should try to

avoid that, because it can mess up the Unix commands.







IMPORTANT COMMANDS




ls ---- list contents of current directory




ls DIRNAME ---- list contents of directory whose name is specified




cp FILENAME1 FILENAME2 ---- copy filename1, call copy "filename2"




mv FILENAME1 FILENAME2 ---- rename filename1, to filename2




mv FILENAME DIRNAME ---- move the file into the directory




rm FILENAME ---- remove ("delete") the file




cd DIRNAME ---- change your current directory to the one specified




cd ---- teleport directly to your account's "home directory"




cd .. ---- climb into your current directory's parent directory




mkdir DIRNAME ---- create a new directory ("folder")




rmdir DIRNAME ---- remove (delete) a directory (must be empty)




logout ---- log out of your account.

            (Especially, if you are using a public computer!)




exit ---- is like logout.

          can be used in certain cases when logout doesn't work




passwd ---- change your account's password




msgs ---- display postings from rohan's electronic message board.

      It will also list some commands, such as "y" or "n" to read or skip

      current message, or "q" to exit the message board.  (If you

      log on to your account, you might see the words "There are new messages".

      That just means someone has posted new messages on rohan's message board.

      Note: You don't need to read rohan's message board, it is

      independent of our class.)




"Control C" ---- holding down the "Ctrl" key and then simultaneously

   pressing the "c" key, will cancel a Unix command that you

   are in the middle of typing.  "Control C" is also used to "break" a program

   (stop it), which is useful if you want to end the program early,

   or if it gets stuck in an infinite loop.  (Note: in certain complex

   situations, it might not stop a program at all, or may be subject to a delay

   before the program actually does stop.)




cat FILENAME ---- show entire contents of the file




more FILENAME ---- show contents of the file, one screen at a time.

     Useful for long files.  Hit "spacebar" to see next screen,

     or "q" to quit early.  Extra commands include "f" to jump forward

     one screen, and "b" to jump backward one screen.




alias rm rm -i ---- Unix will ask you for confirmation ("yes/no")

      before deleting a file.  This prevents accidental file deletions.

      (Note: this command is valid only until you log out,

      you'll need to retype it the next time

      you log on.  There is a way to make it valid even after you log out,

      but this is an advanced issue and will be discussed in class later.)




      Similarly:  alias cp cp -i, alias mv mv -i, etc.




stty erase ^H ---- activates your Backspace key, if your

       terminal or connection causes your Backspace key to generate "^H"

       characters instead of backspacing.  (You can type the "^H" by

       just hitting your backspace key and letting it output the "^H" for you.

       The same procedure holds if your backspace key generates "^?"

       instead of "^H".)

       (Note: the stty command is valid only until you log out,

       so you'll need to retype it the next time you log on.

       There is a way to make it valid even after you log out,

       but this is an advanced issue and will be discussed in class later.)




~ ---- tilde ---- shortcut name for your "home directory"




.. ---- two periods ---- shortcut name for your

          current directory's "parent directory"




. ---- one period ---- shortcut name for your "current directory"




pico FILENAME ---- create or edit a file, using the "pico" text editor

vi FILENAME ---- create or edit a file, using the "vi" text editor




elm ---- start the "elm" email software

mutt ---- start the "mutt" email sofware

pine ---- start the "pine" email software







\-------------------------------------------------------------------------







INTRO TO JAVA

\-------------







STEP 1.  TYPE YOUR PROGRAM




If using Unix's pico text editor, type:

pico Prog1.java

Inside your Prog1.java file, put the following text:




public class Prog1

{




public static void main(String args[])

{

System.out.println("Hello");

}




}




Be careful, some of those are braces "{ }" and some of those

are parentheses "( )".  It might be hard to tell them apart, so look carefully.




To exit your pico file:

* Control X to exit

* Type y or n, depending on whether you want to save your session's changes.

* Hit ENTER or RETURN to accept the filename shown,

  or choose a new filename to save, if you preger.







STEP 2.  COMPILE YOUR PROGRAM




javac Prog1.java




If your program contains no syntax errors (typos in your commands),

then the Java compiler (named "javac") will compile your program into

Java bytecode and store the Java bytecode in a new file named Prog1.class

(which will automatically appear in your directory).




Java bytecode is the gibberish-looking, machine-language version of

your program, written in the machine language of the imaginary

Java Virtual Machine computer.







STEP 3.  RUN YOUR PROGRAM




java Prog1




(A ".class" extension will be automatically added to whatever you type.)

When your Prog1 program executes (runs),

it will output the message "Hello" onto your computer screen.







STEP 4.  IF YOU WANT TO, CREATE AN OUTPUT FILE




When you run your Java program,

it will output the word "Hello" onto your monitor.

To capture a program's output to a file (for example, to print it on paper),




java Prog1 &gt; Prog1.output




The file Prog1.output will automatically be

created, and all the program's output will be sent into that file instead

of to the monitor.







\-------------------------------------------------------------------------







INTRO TO PICO

\-------------




THE "PICO" TEXT EDITOR




UNIX machines have various text editors: pico, vi, emacs, etc.




To create a new file, or to edit an existing file, just type




pico FILENAME




at the rohan prompt, where FILENAME is replaced by

whatever filename you want your file to have.







IMPORTANT COMMANDS:




Typing keyboard keys ---- inserts them into your document.




Arrow keys ---- moves the cursor up/down/left/right, inside your document.




Control k ---- deletes the entire line, where your cursor currently is.




Control x ---- Gets ready to save or exit your file.  See below.







SAVING OR EXITING YOUR FILE:




1. When you have completed your typing, issue the "Control x" command

(it is listed as "^X" in pico's help bar at the bottom of the screen).

You can issue the "Control x" by holding down the "CONTROL" key, and then

simultaneously pressing the "x" key.




2. Next, pico will ask you if you want to save your changes:

"Save modified buffer (Answering "No" Will Destroy Changes)?"

Hit the "y" key, if you want your current pico session's edits to be SAVED

to the file you are editing.

Or, hit the "n" key, if you want to DISCARD and CANCEL all the edits made

during your current pico session, i.e. return the file to what it

originally was when you started your pico session.




3. Next, pico will ask you to choose a file name.

By default, your originally-selected file name will be already typed for you,

so you'll only need to answer the question by just hitting the

ENTER or RETURN key.

However, you can manually change the file name, if you want

your edits to be saved under a different filename.




If you want to modify or update your text file, just repeat

the above steps.  E.g., if you type "pico FILENAME" again, and if

a file named "FILENAME" already exists in your current directory,

then pico will start and will load that file so you can edit/modify it.







\-------------------------------------------------------------------------

\-------------------------------------------------------------------------







public class prog1

{




public static void main(String args[])

{

System.out.println("Hello there!");

}




}







\-------------------------------------------------------------------------







import java.util.*;

public class Prog2

{




public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("Enter your name:");

String s = scan.nextLine();

System.out.println("Hello, " + s + "!");




System.out.println("Enter your age:");

int n = scan.nextInt();

System.out.println("Your age is: " + n);




System.out.println("Enter a real number:");

double x = scan.nextDouble();

System.out.println("Your number is: " + x);




}

}







\-------------------------------------------------------------------------







import java.util.*;

public class prog2

{

public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("What is your name?");

String s  =  scan.nextLine();




System.out.println("What is your age?");

int  n  =  scan.nextInt();




System.out.println("Enter a real number:");

double  x  =  scan.nextDouble();




System.out.println("Hello, " + s + "!");

System.out.println("Your age is = " + n);

System.out.println("Your real number is = " + x);




}

}







\-------------------------------------------------------------------------







import java.util.*;

public class HW1

{




public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("Enter a temp in Celsius:");

double c = scan.nextDouble();




double f = 9/5.0 * c + 32;

System.out.println("In fahrenheit, it is = " + f);




}

}







\-------------------------------------------------------------------------







import java.util.*;

public class HW1

{

public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("What is your name?");

String s  =  scan.nextLine();




System.out.println("Hello, " + s + "!");




}

}







\-------------------------------------------------------------------------







public class prog3

{

public static void main(String args[])

{




String s;

int n;

double x;




s = "Hello there";

n = 21;

x = 6.5;




String t = "Greetings";

int nn = 26;

double blah = 16.5;




System.out.println(s);

System.out.println(t);

System.out.println(nn * 10);




}

}







\-------------------------------------------------------------------------







ARITHMETIC OPERATORS




+  addition

-  subtraction

*  multiplication

/  division

%  modulus, modulo, remainder after a division




   9%2 is: 1

   3%5 is: 3







MIXED-MODE EXPRESSIONS




1. Math between two of the same data types,

   results in an answer with that same data type




   3+5 is: 8

   3.0 + 5.0 is: 8.0




2. Math between two different data types,

   causes the "weaker" data type (such as int)

   to get temporarily upgraded to the "stronger"

   data type (such as double),

   then Rule 1 kicks in.




   3 + 5.0  ---&gt;  3.0 + 5.0 ---&gt;  8.0




   BEWARE OF INTEGER/INTEGER DIVISION!!!




   9 / 2  ----&gt;  normally 4.5, but Rule 1 forces the

                 answer to be an int.




          ----&gt;  4   (truncation, fraction cuts off,

                     does not round to 5)




   9.0 / 2

   9 / 2.0

   9.0 / 2.0




   9.0 / 2  ---&gt; by Rule 2, changes to:  9.0 / 2.0

            ---&gt; Rule 1 kicks in, 9.0 / 2.0 is:  4.5










MIXED-MODE ASSIGNMENT







int variable = int number;      no problem

double variable = double number;     no problem




int variable = double number;    PROGRAM CRASH!




double variable = int number;   no problem,

                                number automatically

                                "casts" (ie, changes)

                                to double, temporarily




int variable = (int) double number;

int x = (int) 6.5;

        ------------- is  6







\-------------------------------------------------------------------------







import java.util.*;

public class prog10

{

public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("Think of an integer number,");

System.out.println("then multiply it by 10,");

System.out.println("then add 8.");

System.out.println("Now type the current number:");




int n = scan.nextInt();




int first;

first = (n-8)/10;




System.out.println("Your first number was: " + first);




}

}







\-------------------------------------------------------------------------







import java.util.*;

public class prog11

{

public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("What is your age?");

int n = scan.nextInt();




if (n &lt; 16)

{

   System.out.println("Can't drive");

}







}

}







\-------------------------------------------------------------------------







import java.util.*;

public class prog12

{

public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("What is your age?");

int n = scan.nextInt();







if (n &lt; 16)

{

   System.out.println("Can't drive");

}

else

{

   System.out.println("Can enter the driving competition");

}







}

}







\-------------------------------------------------------------------------







import java.util.*;

public class prog13

{

public static void main(String args[])

{

Scanner scan = new Scanner(System.in);







System.out.println("What is your age?");

int n = scan.nextInt();




if (n &lt; 0)

{

   System.out.println("Illegal input");

}

else if (n==1 || n==2)

{

  System.out.println("Smart kid");

}

else if (n &lt; 16)

{

  System.out.println("Can't drive");

}

else

{

   System.out.println("Can enter the driving competition");

}







}

}







\-------------------------------------------------------------------------







public class prog20

{

public static void main(String args[])

{







for (int i=1; i&lt;=5; i++)

{

System.out.println("Hello");

}







}

}







\-------------------------------------------------------------------------







public class prog21

{

public static void main(String args[])

{







for (int i=0; i&lt;=8; i=i+2)

{

System.out.println("Hello");

}







}

}







\-------------------------------------------------------------------------







public class prog22

{

public static void main(String args[])

{







for (int i=0; i&lt;=8; i=i+2)

{

System.out.println(i);

}







}

}







\-------------------------------------------------------------------------







public class prog23

{

public static void main(String args[])

{







for (int i=100;  i &gt;= 50  ; i=i-2)

{

System.out.println(i);

}







}

}







\-------------------------------------------------------------------------







public class prog24

{

public static void main(String args[])

{







for (int i=0; i &lt;=50; i=i+2)

{

System.out.println(100-i);

}







}

}







\-------------------------------------------------------------------------







public class prog24x

{

public static void main(String args[])

{







for (int i=0; i &lt;= 25 ; i++)

{

System.out.println(100-2*i);

}







}

}







\-------------------------------------------------------------------------







OUTPUTTING

\----------




System.out.println("Hello");

   outputs the specified text,

   then forces a new line on the screen




System.out.print("Hello");

   outputs the specified text,

   but keeps the screen cursor at the end of the line of text,

   so that the next thing that gets outputted is tacked onto the end of

   the specified text







\-------------------------------------------------------------------------

\-------------------------------------------------------------------------
