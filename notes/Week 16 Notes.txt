


OOP, CONTINUED

==============


POLYMORPHISM:
\-------------
Promotes "extensibility" (not the same as Java's extends keyword).


POLYMORPHISM
\------------

Way #1:
"extends  +  ONE class"

Way #2:
"implements  +  ONE OR MORE interfaces (hollow classes)"

Way #3:
Combine Way#1 and Way#2.

E.g.:
Way#1:   public class Wolf  extends Canine
Way#2:   public class Droid  implements GameCharacter
Way#3:   public class Superwolf  extends Wolf implements GameCharacter, Animal


* Note:
If a class "implements" an interface,
then it MUST code each of the instance methods
that are declared in that interface, or else Java will give you an error.
So, since we said
   public class Droid implements GameCharacter
then we MUST
* create a "public void drawOnScreen()" routine, and must
* create a "public void makeSound()" routine.


\----------------------


//filename: GameCharacter.java

public interface GameCharacter
{

public void drawOnScreen();   //the "instance methods" common to all
                              //game characters, but no code for them.
public void makeSound();

//interfaces can also have public class variables defined as constants:
public final static double PI = 3.14;

//[the newest compilers also allow some extra things in an interface,
//but these new compilers aren't yet installed on rohan.]

}


\----------------------------------------------------------------------------


//filename: Droid.java

public class Droid implements GameCharacter
{

public int x;
public int y;

public Droid()
{
   this.x = 0;
   this.y = 0;
}

public Droid(int t1, int t2)
{
   this.x = t1;
   this.y = t2;
}

public void drawOnScreen()
{
   System.out.println("Now drawing a Droid at coords "+this.x+", "+this.y);
}

public void makeSound()
{
   System.out.println("Beep!");
}


}


\----------------------------------------------------------------------------


//filename: Ufo.java

public class Ufo implements GameCharacter
{

public int x;
public int y;

public Ufo()
{
   this.x = 0;
   this.y = 0;
}

public Ufo(int t1, int t2)
{
   this.x = t1;
   this.y = t2;
}

public void drawOnScreen()
{
   System.out.println("Now drawing a Ufo at coords "+this.x+", "+this.y);
}

public void makeSound()
{
   System.out.println("Breeeeeeee!");
}


}


\----------------------------------------------------------------------------


//filename: StarCruiser.java

public class StarCruiser implements GameCharacter
{

public int x;
public int y;

public StarCruiser()
{
   this.x = 0;
   this.y = 0;
}

public StarCruiser(int t1, int t2)
{
   this.x = t1;
   this.y = t2;
}

public void drawOnScreen()
{
   System.out.println("Now drawing a StarCruiser at coords "
                      +this.x+", "+this.y);
}

public void makeSound()
{
   System.out.println("Brrrrrmmm!");
}


}


\----------------------------------------------------------------------------


//filename: Prog1.java

public class Prog1
{
public static void main(String args[])
{


GameCharacter[] enemies = new GameCharacter[5];

enemies[0] = new Droid(10,10);
enemies[1] = new Ufo(100,10);
enemies[2] = new Ufo(10,100);
enemies[3] = new StarCruiser(60,60);
enemies[4] = new Droid(10,80);


for (int i=0; i&lt;=4; i++)
{
   enemies[i].drawOnScreen();      //polymorphic function calls
   enemies[i].makeSound();         //
}


}
}


\----------------------------------------------------------------------------


THE GAME, WITH NON-POLYMORPHIC ROUTINES
\---------------------------------------


//filename: Prog1.java
public class Prog1
{
public static void main(String args[])
{


GameCharacter[] enemies = new GameCharacter[5];

enemies[0] = new Droid(10,10);
enemies[1] = new Ufo(100,10);
enemies[2] = new Ufo(10,100);
enemies[3] = new StarCruiser(60,60);
enemies[4] = new DarthVader(10,80); //new game character


for (int i=0; i&lt;=4; i++)
{
   enemies[i].drawOnScreen();      //polymorphic function calls
   enemies[i].makeSound();         //


   if (enemies[i] instanceof DarthVader)
   {
      ( (DarthVader)enemies[i] ).swingsaber();   //non-poly function call
   }
}


}
}


\----------------------------------------------------------------------------
\----------------------------------------------------------------------------



OPTIONAL LECTURES:  GUI
=======================



QUICK INTRO TO GUI PROGRAMMING
\------------------------------

SDSU schedules GUI programming in the 3rd semester (and later) classes,
but here are some quick intros to the topic.


\---


//filename: Gprog1.java

/*

* JFrame is like a Java window,
  which here we fill with a JPanel that has the graphics.

* JPanel is a basic element for easily showing graphics, GUI, etc.

* Whenever your computer has to show a JPanel on the screen,
  Java automatically calls the JPanel's paintComponent subroutine.
  So, that's where you can put program statements to
  set what the JPanel looks like.

* The rest of the program is exactly what you think it is:
  setting colors, painting rectangles, painting text.

* But, if you have to center something, like a circle,
  that's tricky, since the Java panel or window size
  isn't exactly what you request, since Java windows get borders, etc.
  So, you ought to compute the panel or window's size.

*/


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gprog1
{
public static void main(String[] args)
{

   Gprog1panel p = new Gprog1panel();

   JFrame window = new JFrame("GUI Test");
      window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      window.setContentPane(p);
      window.setSize(400,400);
      window.setLocation(100,100);
      window.setVisible(true);

} //main
} //class


class Gprog1panel extends JPanel
{

   public void paintComponent(Graphics g)
   {
      super.paintComponent(g);

      this.setBackground(Color.WHITE);

      g.setColor(Color.BLACK);
      float panelW = this.getWidth();
      float panelH = this.getHeight();
      g.fillOval(0, 0, (int)panelW, (int)panelH);

      g.setColor(Color.BLUE);
      g.fillRect(25,25,100,30);

      g.setColor(new Color(128,128,128));
      g.fillRect(25,25,100,20);

      g.setColor(Color.RED);
      g.drawString("Hello", 25,120);

      g.drawString("panel W,H = " + panelW + "," + panelH, 25,140);
   }

} //class


\----------------------------------------------------------------------------


//filename: progclk.java

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.net.*;

public class progclk
{

public static void main(String[] args)
{

   final String s1 = "Hi";   //choose button 1's text
   final String s2 = "Hello";   //choose button 2's text

   Runnable r1 = new Runnable() {
      public void run()
      {
         progclk.createAndShowGUI(s1, s2);
      }
   };
   SwingUtilities.invokeLater(r1);

} //main


public static void createAndShowGUI(String s1, String s2)
{
    //Create and set up the window:
    JFrame frame = new JFrame("GUI Program");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //Create and set up the content pane:
    Clk2 newContentPane = new Clk2(s1, s2);
    frame.setContentPane(newContentPane);
    newContentPane.setOpaque(true);

    //Show window:
    frame.pack();
    frame.setVisible(true);
}


} //class



class Clk2 extends JPanel implements ActionListener
{

private JButton b1, b2;

public Clk2(String s1, String s2)
{
   ImageIcon b1icon = new ImageIcon("pic1.jpg");   //choose button 1's image
      b1 = new JButton(s1, b1icon);
      b1.setActionCommand("b1");
      b1.addActionListener(this);
      this.add(b1);

   ImageIcon b2icon = new ImageIcon("pic2.jpg");   //choose button 2's image
      b2 = new JButton(s2, b2icon);
      b2.setActionCommand("b2");
      b2.addActionListener(this);
      this.add(b2);
}


public void actionPerformed(ActionEvent e)
{
   String str1 = e.getActionCommand();   //is "b1" or "b2", re what was clicked

   if (str1.equals("b1"))
   {
      String str2 = "Hi from button 1!";
      JOptionPane.showMessageDialog(null, str2);
   }
   else if (str1.equals("b2"))
   {
      String str2 = "Hello from button 2!";
      JOptionPane.showMessageDialog(null, str2);
   }
}


} //class


\----------------------------------------------------------------------------


HTML5: In file prog100.html
\---------------------------


&lt;html&gt;

&lt;head&gt;

&lt;script type="text/javascript"&gt;


//globals
var canvas;
var ctx;
var gameloop;
var n;


function init()
{
  canvas = document.getElementById("canvas1");
  if (! canvas.getContext) return;
  ctx = canvas.getContext("2d");

  showitems();
  n=0;
  gameloop=setInterval(change,100);
}


function showitems()
{
  ctx.fillStyle="blue";
  ctx.fillRect(10,10,100,100);
  ctx.fillStyle="rgba(0,255,0,.5)";
  ctx.fillRect(60,60,100,100);

  ctx.beginPath();
  ctx.moveTo(200,200);
  ctx.lineTo(300,100);
  ctx.lineTo(400,200);
  ctx.lineTo(200,200);
  ctx.closePath();
  ctx.fillStyle="white";
  ctx.fill();
  ctx.strokeStyle="black";
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(100,300,90, 0,2*Math.PI, true);
  ctx.closePath();
  ctx.fillStyle="yellow";
  ctx.fill();
}


function routine1()
{
  alert("Thanks for clicking!");

  ctx.beginPath();
  ctx.moveTo(200,200);
  ctx.lineTo(300,100);
  ctx.lineTo(400,200);
  ctx.lineTo(200,200);
  ctx.closePath();

  ctx.fillStyle="blue";
  ctx.fill();
  ctx.strokeStyle="blue";
  ctx.stroke();

  clearInterval(gameloop);
}


function change()
{
  ctx.font="italic 80px Courier New";
  if (n==0)
  {
     ctx.fillStyle="blue";
     n=1;
  }
  else if (n==1)
  {
     ctx.fillStyle="green";
     n=0;
  }

  ctx.fillText("Hello",100,100);
}


&lt;/script&gt;

&lt;/head&gt;

&lt;body onload="init()"&gt;

&lt;h1&gt;Sample&lt;/h1&gt;

This is a sample&lt;br&gt;

&lt;canvas id="canvas1" width="400" height="400" onclick="routine1()"&gt;
Browser can't show canvas, enable Javascript!
&lt;/canvas&gt;

&lt;br&gt;
This is a sample.&lt;br&gt;
Try clicking the canvas!&lt;br&gt;

&lt;/body&gt;

&lt;/html&gt;


\----------------------------------------------------------------------------


CREATING CLICKABLE "EXECUTABLES" FOR WINDOWS, ETC.
\--------------------------------------------------


WAY #1:  CLICKABLE GUI PROGRAMS
\-------

If you create a GUI program such as the prior GProg1 or progclk programs,
you can create a clickable "executable" for Windows, etc.
(It's not really an executable, it just runs your program with an icon click.)

1. Compile your GProg1.java file, to get GProg1.class and Gprog1panel.class

2. Create a new text file named GProg1.mf, with this text in it:
   Main-Class: GProg1
   [put a blank line here]

3. Create a JAR file:

   jar cmf GProg1.mf GProg1.jar GProg1.class Gprog1panel.class

   note: you can also include the .java files, but that's optional

4. If you want, you can run it on your rohan account:  (but rohan might require
                                            a graphical connection like Xwin32)
   java -jar GProg1.jar

5. Download the JAR file to a PC or computer that has the JRE installed.

6. On the PC, double-click the GProg1 icon, to launch/run the GUI program!



WAY #2:  SEMI-CLICKABLE TEXT PROGRAMS
\-------

If your program is a text program, like a CS107 or CS108 program,
your computer can't launch it directly from an icon double-click,
since the JAR icon double-click runs javaw.exe, not java.exe,
and so it doesn't open the text window that shows your text program.

But, you can instead create a clickable "batch file",
which basically types the "java Prog1" command for you.

1. Compile your Prog1.java file, to get Prog1.class

2. Create a new text file named Prog1.mf, with this text in it:
   Main-Class: Prog1
   [put a blank line here]

3. Create a JAR file:

   jar cmf Prog1.mf Prog1.jar Prog1.class

   note: you can also include the .java files, but that's optional

4. If you want, you can run it on your rohan account:
   java -jar Prog1.jar

5. Download the JAR file to a PC or computer that has the JRE installed.

6. On the PC, create a text file with a .bat file extension, like Prog1.bat,
   and put this text:

   java -jar Prog1.jar

   Note: the PC will need its system Path set to include the JRE folder.
 
7. On the PC, double-click the Prog1.bat file, to launch/run the text program.

   Note: doing this for a GUI program will also work, except the text window
   will show up while the GUI program is running.

   Note: the text window closes when your program is complete,
   so you might want to include something at the end of your program,
   such as a "Hit Enter" input line.


\----------------------------------------------------------------------------
\----------------------------------------------------------------------------






