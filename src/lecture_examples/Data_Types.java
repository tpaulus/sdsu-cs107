package lecture_examples;

/**
 * @author Tom Paulus
 *         Created on 9/9/15.
 */

public class Data_Types {
    public static void main(String[] args) {
        String s;
        int n;
        double x;

        s = "Hello There!";
        n = 21;
        x = 6.5;

        String t = "Greetings!";
        int nn = 26;
        double blah = 16.5;

        System.out.println(s);
        System.out.println(t);
        System.out.println(nn * 10);
    }
}
