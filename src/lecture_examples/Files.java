package lecture_examples;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Tom Paulus
 *         Created on 10/21/15.
 */
public class Files {
    private static List<String> listContents(final String path) throws IOException{
        final File file = new File(path);
        final List<String> resultList = new ArrayList<>();

        final File[] fList = file.listFiles();
        if (fList != null) {
            for (File f : fList) {
                resultList.add(f.getCanonicalPath());

                if (f.isDirectory()) {
                    resultList.addAll(listContents(f.getCanonicalPath()));
                }
            }
        }
        return resultList;
    }

    public static void main(String[] args) throws IOException {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Please input a file pattern:");
        final String pattern = scanner.nextLine();


        for (String fileName : listContents(".")) {
            if (fileName.contains(pattern)) {
                System.out.println(fileName);
            }
        }
    }
}
