package lecture_examples;

import java.util.Scanner;

/**
 * @author Tom Paulus
 *         Created on 9/14/15.
 */
public class Guess_My_Number {
    private static int undo(int modified){
        return (modified - 8)/10;
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int modified_number;

        System.out.println("Think of an integer number...");
        System.out.println("Multiply the number by 10");
        System.out.println("Now add 8.");
        System.out.println("Type in your final result.");
        modified_number = scanner.nextInt();

        System.out.printf("Was your number %d?", undo(modified_number));
    }
}
