package lecture_examples;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Let's get some user input and echo it back.
 *
 * @author Tom Paulus
 *         Created on 9/2/15.
 */

public class User_Input {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What's your name?");
        final String name = scanner.nextLine();
        System.out.printf("Hi there, %s!", name);

        System.out.println("\n");

        System.out.printf("Where are you from %s?\n", name);
        final String home = scanner.nextLine();
        System.out.printf("That's wonderful, I hear it is really nice in %s!", home);

        System.out.println("\n");

        System.out.printf("What's your favorite number %s? Mine is 42.\n", name);
        try {
            final Integer num = scanner.nextInt();
            if (num % 2 == 0) {
                System.out.println("Both of our favorite numbers are even!! :)");
            } else {
                System.out.println("I guess you're the odd man out. ;)");
            }
        } catch (InputMismatchException e) {
            System.out.println("I haven't heard of that one before... :(");
        }

        System.out.println("\n");

        System.out.println("What is pi?");
        final Double pi = scanner.nextDouble();
        final Integer length = pi.toString().replace(".", "").length(); // Remove decimal point to simplify length calculation
        System.out.printf("\nWow! Your number is %d digits long", length);
    }
}
