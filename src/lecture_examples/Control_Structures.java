package lecture_examples;

import java.util.Scanner;

/**
 * @author Tom Paulus
 *         Created on 9/14/15.
 */
public class Control_Structures {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int user_age;

        System.out.println("What is your age?");
        user_age = scanner.nextInt();

        if (user_age < 0) {
            System.out.println("That is an illegal input.");
        } else if (user_age < 16) {
            System.out.println("You are not allowed to Drive. :(");
        } else {
            System.out.println("You are allowed to drive.");
        }

    }

}
