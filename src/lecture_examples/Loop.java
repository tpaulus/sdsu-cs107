package lecture_examples;

/**
 * @author Tom Paulus
 *         Created on 9/16/15.
 */
public class Loop {
    public static void main(String[] args) {
        for (int i=1; i<=5; i++){
            System.out.println("Hello!");
        }
        for (int i=100; i>=50; i-=2){
            System.out.println(i);
        }
    }
}
