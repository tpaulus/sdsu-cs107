package homework;

import java.util.Scanner;

/**
 * Homework Assignment #6
 * Assigned October 30, 2015
 * Objective: Getting Familiar with Modular Programming
 * Specs: http://rohan.sdsu.edu/~masc0555/cs107hw6.txt
 *
 * @author Tom Paulus
 *         Created on 10/31/15.
 */

public class HW6 {
    /**
     * Get Valid Score from the user.
     *
     * @return Valid Input
     */
    private static int getLegalInput() {
        int userInput;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input an exam score (between 0 and 100), or -1 to stop:");
        while (true) {
            userInput = scanner.nextInt();
            if (-1 <= userInput && userInput <= 100) {
                return userInput;

            } else {
                System.out.printf("%d was not a valid exam score.\n" +
                        "Please input an valid exam score, or -1 to stop.\n", userInput);
            }
        }
    }

    public static void main(String[] args) {
        int userInput;

        int scoresSum = 0;
        int scoresEntered = 0;

        do {
            userInput = getLegalInput();

            if (userInput != -1) {
                // Check to make sure that the input is a score, then add it to the sum and increment the counter
                scoresSum += userInput;
                scoresEntered++;
            }
        }
        while (userInput != -1);

        // Process the input to produce the Average Score
        if (scoresEntered > 0) {   // Ensure that at least one score was entered to ensure that we don't divide by 0.
            double examScoreAverage = (double) scoresSum / scoresEntered;
            System.out.printf("Your average score was %.1f", examScoreAverage);

        } else {
            System.out.println("You never entered any exam scores. :(");
        }
    }

}
