package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Homework Assignment #7
 * Assigned November 13, 2015
 * Objective: Using Modular Programming and Loops to Calculate a CSS
 * Specs: http://rohan.sdsu.edu/~masc0555/cs107hw7.txt
 *
 * Note: Common Sum Square is also referred to as a "Magic Square" - https://en.wikipedia.org/wiki/Magic_square
 *
 * @author Tom Paulus
 *         Created on 11/14/15.
 */
public class HW7 {
    static final int SIZE = 3;
    static final int CSS_SUM = 15;
    static final int U_POS = 0; // Position to insert the user supplied starting value

    /**
     * Print out the Common Sum
     *
     * @param fields {@link int[]} array to print out.
     */
    public static void printCSS(final int[] fields) {
        System.out.printf(" %d | %d | %d\n" +
                        "---+---+---\n" +
                        " %d | %d | %d\n" +
                        "---+---+---\n" +
                        " %d | %d | %d\n",
                fields[0], fields[1], fields[2],
                fields[3], fields[4], fields[5],
                fields[6], fields[7], fields[8]);

        System.out.printf("Common Sum: %d\n\n", CSS_SUM);
    }

    /**
     * Check is the supplied array is in fact a Common Sum Square
     *
     * @param css {@link int[]} the proposed common sum square
     * @return {@link boolean} if the the supplied array is in fact a CSS.
     */
    private static boolean checkSums(final int[] css) {
        // Check Rows
        for (int r = 0; r < SIZE; r++) {
            int sum = 0;
            for (int row_element = 0; row_element < SIZE; row_element++) {
                sum += css[r * SIZE + row_element];
            }
            if (sum != CSS_SUM) {
                return false;
            }
        }

        // Check Cols
        for (int c = 0; c < SIZE; c++) {
            int sum = 0;
            for (int col_element = 0; col_element < SIZE; col_element++) {
                sum += css[c + SIZE * col_element];
            }
            if (sum != CSS_SUM) {
                return false;
            }
        }

        // Check Diags
        int sum;
        sum = 0;
        for (int diag_element = 0; diag_element < SIZE; diag_element++) {
            sum += css[(SIZE + 1) * diag_element];
        }
        if (sum != CSS_SUM) {
            return false;
        }
        sum = 0;
        for (int diag_element = 0; diag_element < SIZE; diag_element++) {
            sum += css[(SIZE - 1) + ((SIZE - 1) * diag_element)];
        }
        if (sum != CSS_SUM) {
            return false;
        }
        return true;
    }

    /**
     * Generate all possible permutations of length k.
     *
     * @param k {@link int} Length of the array for each permutation.
     *          Should be greater than 0.
     * @return {@link List} of int arrays, containing all permutations of the digits 1..k;
     */
    private static List<int[]> calculatePermutations(final int k) {
        assert (k > 0);

        final List<int[]> r_list = new ArrayList<>();
        if (k == 1) {
            r_list.add(new int[]{1});
        } else {
            final List<int[]> p_list = calculatePermutations(k - 1);
            for (final int[] p : p_list) {
                for (int pos = 0; pos < k; pos++) {
                    final int[] new_permutation = new int[k];
                    new_permutation[pos] = k;
                    for (int i = 0; i < k - 1; i++) {
                        new_permutation[i < pos ? i : i + 1] = p[i];
                    }
                    r_list.add(new_permutation);
                }
            }
        }

        return r_list;
    }

    /**
     * Calculate all possible Common Sum Squares that have the supplied number in the pre-set position
     *
     * @param ui {@link int} The value for the square at U_POS
     * @return {@link List} of all CSS that meet the criteria
     */
    private static List<int[]> calculateCSS(final int ui) {
        final List<int[]> candidates = new ArrayList<>();
        final List<int[]> css_list = new ArrayList<>();

        final List<int[]> permutations = calculatePermutations(SIZE * SIZE);
        for (final int[] p : permutations) {
            if (p[U_POS] == ui) {
                candidates.add(p);
            }
        }

        for (final int[] c : candidates) {
            if (checkSums(c)) {
                css_list.add(c);
            }
        }
        return css_list;
    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        System.out.printf("Input upper-left digit, 1 to %d:\n", (SIZE * SIZE));
        final int input = scanner.nextInt();
        if (0 < input && input <= (SIZE * SIZE)) {
            final List<int[]> results = calculateCSS(input);
            if (results.size() > 0) {
                for (final int[] r : results) {
                    System.out.print("\n");
                    printCSS(r);
                }
            } else {
                System.out.println("CSS not possible");
            }

        } else {
            System.out.println("Invalid Input");
        }
    }
}
