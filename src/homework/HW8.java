package homework;

import java.util.Scanner;

/**
 * Homework Assignment #8
 * Assigned November 28, 2015
 * Objective: Using Modular Programming and Loops to Calculate a Sin
 * Specs: http://rohan.sdsu.edu/~masc0555/cs107hw8.txt
 *
 * @author Tom Paulus
 *         Created on 11/30/15.
 */

public class HW8 {
    /**
     * Calculate the Factorial of X
     *
     * @param x {@link int} X
     * @return {@link long} Value of x!
     */
    private static long factorial(int x) {
        long f = 1;

        for (int i = 1; i <= x; i++) {
            f *= i;
        }

        return f;
    }

    /**
     * Calculate the Sine Value
     *
     * @param r    {@link double} value of x in radians
     * @param epow {@link int} largest exponent to use in calculations
     * @return {@link double} Sine value of r
     */
    private static double sin(double r, int epow) {
        double x = 0;
        int sign = 1;

        for (int p = 1; p <= epow; p += 2) {
            x += sign * (Math.pow(r, p) / factorial(p));
            sign *= -1; // Alternate the sign for the next iteration
        }

        return x;
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        System.out.print("Value of X (in Radians): ");
        double r = scanner.nextDouble();

        System.out.print("Maximum Exponent (The Greater, The Greater the Accuracy): ");
        int maxExp = scanner.nextInt();

        System.out.println("The approximated sine value is " + sin(r, maxExp));
    }
}
