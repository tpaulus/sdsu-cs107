package homework;

import java.util.Scanner;

/**
 * http://www-rohan.sdsu.edu/~masc0555/cs107hw2.txt
 *
 * @author Tom Paulus
 *         Created on 9/13/15.
 */
public class HW2 {
    public static void main(String[] args) {
        final double CONCERT_TICKET_PRICE = 20.00;
        final double MOVIE_TICKET_PRICE = 9.00;
        String user_name = "";
        int movie_ticket_count = -1;
        int concert_ticket_count = -1;
        double order_total = 0d;

        final Scanner scanner = new Scanner(System.in);


        System.out.println("Welcome to On-Line Tickets!");

        do {
            try {
                while (user_name.isEmpty()) {
                    System.out.println("What is your name?");
                    user_name = scanner.nextLine().trim();
                }

                while (movie_ticket_count < 0) {
                    System.out.printf("How many movie tickets ($%2.2f) would you like to purchase?\n", MOVIE_TICKET_PRICE);
                    movie_ticket_count = scanner.nextInt();
                }

                while (concert_ticket_count < 0) {
                    System.out.printf("How many concert tickets ($%2.2f) would you like to purchase?\n", CONCERT_TICKET_PRICE);
                    concert_ticket_count = scanner.nextInt();
                }

                System.out.printf("Your order has been processed %s.\n", user_name);

                order_total = (MOVIE_TICKET_PRICE * movie_ticket_count) + (CONCERT_TICKET_PRICE * concert_ticket_count);
                System.out.printf("Total Order: $%2.2f\n", order_total);
            } catch (Exception e) {
                System.out.println("Looks like you made an invalid entry, let's try again.");
                scanner.nextLine();
            }

        } while (order_total == 0);
    }
}
