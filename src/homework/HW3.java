package homework;

import java.util.Scanner;

/**
 * Homework Assignment #3
 * Assigned on September 26, 2015
 * Objective: To convert temperatures from Celsius to Fahrenheit, and vise-versa.
 * Specs: http://www-rohan.sdsu.edu/~masc0555/cs107hw3.txt
 *
 * @author Tom Paulus
 *         Created on 9/26/15.
 */

public class HW3 {
    /**
     * Convert Celsius to Fahrenheit
     *
     * @param c {@link Double} Celsius temperature to convert.
     * @return {@link Double} Fahrenheit temperature equivalent to c.
     */
    static Double c_to_f(final Double c) {
        return c * 1.8 + 32;
    }

    /**
     * Convert Fahrenheit to Celsius
     *
     * @param f {@link Double} Fahrenheit temperature to convert.
     * @return {@link Double} Celsius temperature equivalent to f.
     */
    static Double f_to_c(final Double f) {
        return (f - 32) / 1.8;
    }

    /**
     * Convert Celsius to Kelvin
     *
     * @param c {@link Double} Celsius temperature to convert.
     * @return {@link Double} Kelvin temperature equivalent to c.
     */
    static Double c_to_k(final Double c) {
        return c + 273.15;
    }

    /**
     * Convert Fahrenheit to Kelvin
     *
     * @param f {@link Double} Fahrenheit temperature to convert.
     * @return {@link Double} Kelvin temperature equivalent to f.
     */
    static Double f_to_k(final Double f) {
        return c_to_k(f_to_c(f));
    }

    /**
     * Check whether a temperature is possible in the Physical Universe.
     *
     * @param t {@link Double} Temperature to Verify.
     * @param unit {@link String} One Character abbreviation for the unit that t is in.
     * @return {@link Boolean} If the temperature is possible.
     */
    static Boolean valid_temp(final Double t, final String unit) {
        assert unit.equals("c") || unit.equals("f") || unit.equals("k");
        final Double check_temp;
        switch (unit) {
            case "c":
                check_temp = c_to_k(t);
                break;
            case "f":
                check_temp = f_to_k(t);
                break;
            default:
                check_temp = t;
                break;
        }
        return check_temp > 0;
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        System.out.println("Select conversion type:\n" +
                "1 = celsius to fahrenheit\n" +
                "2 = fahrenheit to celsius");
        final String mode = scanner.nextLine();


        final Double input_temp;
        switch (mode.toLowerCase()) {
            case "1":
                System.out.println("Please Enter a Celsius Temperature:");
                input_temp = scanner.nextDouble();

                if (valid_temp(input_temp, "c")) {
                    System.out.printf("That's the same as %1$,.1f Fahrenheit", c_to_f(input_temp));
                } else {
                    System.out.println("Now that's interesting... Temperatures that low aren't possible.");
                }
                break;

            case "2":
                System.out.println("Please Enter a Fahrenheit Temperature:");
                input_temp = scanner.nextDouble();

                if (valid_temp(input_temp, "f")) {
                    System.out.printf("That's the same as %1$,.1f Celsius", f_to_c(input_temp));
                } else {
                    System.out.println("Now that's interesting... Temperatures that low aren't possible.");
                }
                break;

            default:
                System.out.println("That isn't a valid conversion method...");
                break;

        }
    }
}
