package homework;

/**
 * Homework Assignment #5
 * Assigned October 20, 2015
 * Objective: Using for loops to perform mathematical operations
 * Specs: http://www-rohan.sdsu.edu/~masc0555/cs107hw5ss.txt
 *
 * @author Tom Paulus
 *         Created on 10/21/15.
 */
public class HW5 {
    /**
     * Sum all the numbers from 1 to n.
     *
     * @param n Stop Point of the Sum
     * @return the Sum of all integers between 1 and n.
     */
    private static int largeSum(final int n) {
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum += i;
        }

        return sum;
    }

    /**
     * Find the factorial of a given number. Computes and returns n!
     *
     * @param n Find the Factorial of
     * @return Factorial of n
     */
    private static int factorial(final int n) {
        int factorial;
        if (n > 0) {
            factorial = 1;
            for (int i = 1; i <= n; i++) {
                factorial *= i;
            }
        } else {
            factorial = 0;
        }

        return factorial;
    }

    public static void main(String[] args) {
        final int largeSum = largeSum(10);
        if (largeSum == 55) {
            System.out.println(String.format("LARGE SUM\n==========\n\tPASS\n\tReturned: %d", largeSum));
        } else {
            System.out.println(String.format("LARGE SUM\n==========\n\tFAILED\n\tReturned: %d\n\tShould be 55", largeSum));
        }

        System.out.println("\n");

        final int factorial = factorial(10);
        if (factorial == 3628800) {
            System.out.println(String.format("FACTORIAL\n==========\n\tPASS\n\tReturned: %d", factorial));
        } else {
            System.out.println(String.format("FACTORIAL\n==========\n\tFAILED\n\tReturned: %d\n\tShould be 3628800", factorial));
        }
    }
}
