package homework;

import java.util.Scanner;

/**
 * Homework Assignment #4
 * Assigned October 8, 2015
 * Objective: Use Loops and Conditional to process user input
 * Specs: http://www-rohan.sdsu.edu/~masc0555/cs107hw4.txt
 *
 * @author Tom Paulus
 *         Created on 10/10/15.
 */
public class HW4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int userInput;

        int scoresSum = 0;
        int scoresEntered = 0;

        do {
            System.out.println("Input an exam score, or -1 to stop:");
            userInput = scanner.nextInt();

            if (userInput != -1) {
                // Check to make sure that the input is valid, then add it to the sum and increment the counter
                scoresSum += userInput;
                scoresEntered++;
            }
        }
        while (userInput != -1);

        // Process the input to produce the Average Score
        if (scoresEntered != 0) {   // Ensure that at least one score was entered to ensure that we don't divide by 0.
            double examScoreAverage = (double) scoresSum / scoresEntered;
            System.out.printf("Your average score was %.1f", examScoreAverage);

        } else {
            System.out.println("You never entered any exam scores. :(");
        }
    }
}
