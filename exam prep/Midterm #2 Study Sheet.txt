* The Midterm questions will be similar to those given below.


* The answers to the sample questions are included at the
  end of this Study Sheet.

* The real exam's questions will be multiple choice Scantron.

\--------------------------------------------------------------------------


BASIC PROGRAMMING

#1.  Sample question:

Create a program fragment (not the whole program) which does the following.

Suppose that the variable time1 stores the time (in seconds)
that a runner needed, to run around a track.
Suppose also that the variable time2 stores the time (in seconds) that the
runner needed, to run around the track on a later date.

Write a short Java program fragment which displays the message
"Became faster by ## seconds" if the second run was faster than the first,
or displays "Same time" if the second time was the same as the first,
or displays "Became slower by ## seconds" if the second run was slower than
the first.  (Of course, in the actual output, the ##'s above should
be replaced by the actual number of seconds of difference.)

Hint: pay attention to positive and negative values, so that the output
makes sense.  For example:
* If time1 is 10, and time2 is 18, then your program code
  should cause the output to be: "Became slower by 8 seconds",
  and NOT "Became faster by -8 seconds" and NOT "Became slower by -8 seconds".
* If time1 is 20, and time2 is 10, then your program code
  should cause the output to be: "Became faster by 10 seconds",
  and NOT "Became slower by -10 seconds" or etc.



System.out.println("Input time #1:");
int time1 = scan.nextInt();
System.out.println("Input time #2:");
int time2 = scan.nextInt();




int delta = time2 - time1;




if ( delta &gt; 0 ) {

   System.out.println("Became faster by " + delta + " seconds");

} else if ( delta == 0 ) {

   System.out.println("Same time");

} else {

   System.out.println("Became slower by " + (delta * -1) + " seconds");

}


\--------------------------------------------------------------------------


"FOR" LOOPS

#2.  Sample question:

What will be the output generated to the screen upon running
the following program?




public class Prog1 {

     public static void main (String args[]) {

         for (int i=16; i &gt;= 6; i=i-2) {

            System.out.println(5*i);

         }

     }

}




Output:

80

70

60

50

40

30


\--------------------------------------------------------------------------


"WHILE" LOOPS

#3.  Sample question:

What will be the output generated to the screen upon running
the following program?




public class Prog1 {

     public static void main (String args[]) {

         int y = 6;

         int x = 1;




         while (x &lt;= y+2) {

                x = x + 2;

                System.out.println(y);

                 y = y - 1;

         }

     }

}




Output:

6 (x=1)

5 (x=3)

4 (x=5)


\-------------------------------------------------------------------------


"FOR" LOOPS

#4.  Sample question:

What will be the output generated to the screen upon running
the following program?




public class Prog1 {

     public static void main(String args[]) {

         for (int x=1 ; x&lt;=4 ; x++) {

             int y = 10 * x + 1;

             System.out.println("x=" + x + ", y=" + y);

          }

     }

}




Output:

x=1, y=11

x=2, y=21

x=3, y=31

x=4, y=41




\-------------------------------------------------------------------------


"DO-WHILE" LOOPS

#5.  Sample question:

What will be the output generated to the screen upon running
the following program?




public class Prog1 {

     public static void main (String args[]) {

          int x = 2;

          int y = 18;




          do {

               y = y - 2;

               System.out.println(y);

               x = x + 2;

          } while ( 2*x &lt;= y+2 );

     }

}




Output:

16 (x=4)

14 (x=6)

12 (x=8)



\-------------------------------------------------------------------------


WEIRD "FOR" LOOPS

#6.
What will be outputted by the following program code?
[HINT: You can't guess this, you must use the "3 steps of a FOR loop".]


int g = 9;
int k = 3;



for (int x=10; k&lt;g; g--) {

  k = k+2;
  System.out.println(x);
  x++;
  g = g+1;
}




Output:

(k=5) 10 (x=11) (g remains constant at 9)

(k=7) 11 (x=12)

(k=9) 12 (x=13)




\-------------------------------------------------------------------------


NESTED "IF" STRUCTURES

#7.
What will be the output generated to the screen upon running the following
program code?


int a = 10;
int b = 30;

if (b+1 &lt; a*3)
{

       if (a + 50 &gt;= b) {

            System.out.println("Joe");

       } else {

           System.out.println("Jane");

       }

} else if (a*3 &lt;= b+100) {

       if (b*3 &gt;= a*3)  {

           System.out.println("John");

       } else if (b*3 &gt;= a*3) {

           System.out.println("Jim");

       } else  {

           System.out.println("Kim");

       }

} else {

       if (a + 3 &lt;= b)  {

          System.out.println("Jessie");

       }  else  {

          System.out.println("Suzi");

       }
}




Output:

John




\-------------------------------------------------------------------------


NESTED "FOR" LOOPS

#8.
What will be outputted by the following program code?




for (int x=1; x&lt;=5; x=x+2) {

   for (int y=30; y&gt;=28; y--) {

      System.out.println("x=" + x + ", y=" + y);
   }
}




Output:

x=1, y=30

x=1, y=29

x=1, y=28

x=3, y=30

x=3, y=29

x=3, y=28

x=5, y=30

x=5, y=29

x=5, y=28


\-------------------------------------------------------------------------


NESTED "FOR" LOOPS

#9.
What will be outputted by the following program code?




for (int x=10; x&lt;13; x++) {    //caution: look at the 2nd slot carefully

   for (int y=13; y&gt;=x; y—) {  //caution: look at the 2nd slot carefully

      System.out.println("x=" + x + ", y=" + y);
   }
}




Output:

x=10, y=13

x=10, y=12

x=10, y=11

x=10, y=10

x=11, y=13

x=11, y=12

x=11, y=11

x=12, y=13

x=12, y=12




\-------------------------------------------------------------------------


OPTIMIZATION

#10.

You are asked to design a pet enclosure for two pets.
The length is L, and the width is W.
There are two long fence sections, each of length L,
and there are three perpendicular sections, each of length W,
as shown:

      +----------+   .
      |        |       |   W

      +----------+   :

      .....L......

Each section is cut to an integer number of feet,
so L is an integer and W is an integer.
There is 100 ft fencing, but you do not need to
use all of it.

You want to compute the length L and width W
that will yield the largest possible area for the pets.




public class Prog1 {

public static void main(String args[]) {

     int largestAreaSoFar = 0;

     int bestLSoFar = 0;

     int bestWSoFar = 0;




     for (int L=1; L&lt;=50; L++) {

          for (int W=1; W&lt;=50; W++) {

               int currentArea = L * W;

               int currentFencing = 2 * L + 3 * W;




               if ( 100 =&gt; currentFencing  &amp;&amp;  largestAreaSoFar &lt; currentArea ) {

                    largestAreaSoFar = currentArea;

                    bestLSoFar = L;

                    bestWSoFar = W;

               }

           }

     }




     System.out.println( "Use L = " + bestLSoFar );

     System.out.println( "Use W = " + bestWSoFar );

     System.out.println( "Largest area = " + largestAreaSoFar );

     System.out.println( "Total length of fencing = " + 2 *  bestLSoFar + 3 * bestWSoFar );




     } 

} 



\-------------------------------------------------------------------------
\-------------------------------------------------------------------------


END OF PRACTICE EXAM.










ANSWERS ARE INCLUDED BELOW.


\-------------------------------------------------------------------------
\-------------------------------------------------------------------------


#1.  ANSWER:

One of various possible solutions is:

if (time2 &lt; time1)
{
  int a = time1 - time2;
  System.out.println("Became faster by " + a + " seconds");
}
else if (time2 == time1)
{
  System.out.println("Same time");
}
else
{
  int a = time2 - time1;
  System.out.println("Became slower by " + a + " seconds");
}


\------------------------------------------------------------------------


#2. ANSWER

80
70
60
50
40
30


#3. ANSWER

6
5
4


#4. ANSWER

x=1, y=11
x=2, y=21
x=3, y=31
x=4, y=41


#5. ANSWER

16
14
12


\--------------------------------------------------------------------


#6.

10
11
12



#7.

John



#8.

x=1, y=30
x=1, y=29
x=1, y=28
x=3, y=30
x=3, y=29
x=3, y=28
x=5, y=30
x=5, y=29
x=5, y=28



#9.

x=10, y=13
x=10, y=12
x=10, y=11
x=10, y=10
x=11, y=13
x=11, y=12
x=11, y=11
x=12, y=13
x=12, y=12


\--------------------------------------------------------------------


#10.

//LOTS OF POSSIBLE SOLUTIONS.
//EXAMPLE SOLUTION:


public class Prog1
{
public static void main(String args[])
{

int largestareasofar = -999999;
int bestLsofar = -1;
int bestWsofar = -1;

for (int L=1; L&lt;=100; L++)         //OR:  L = 1 TO 48, etc.
{
   for (int W=1; W&lt;=100; W++)      //OR:  etc
   {
      int currentarea = L*W;
      int currentfencing = 2*L + 3*W;

      if ( currentarea &gt; largestareasofar  &amp;&amp;  currentfencing &lt;= 100 )
      {
         largestareasofar = currentarea;
         bestLsofar = L;
         bestWsofar = W;
      }
   }
}

System.out.println("Use L = " + bestLsofar);
System.out.println("Use W = " + bestWsofar);
System.out.println("Largest area = " + largestareasofar);
System.out.println("Total length of fencing = " + (2*bestLsofar+3*bestWsofar));

} //main
} //class










