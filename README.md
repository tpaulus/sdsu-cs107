# CS107 - Fall 2015
### *Taught by Professor Ivan Bajic at [San Diego State University](http://www.sdsu.edu)*


Coursework and material from my CS107 class, taken during the Fall 2015 Semester. This material is made available online for educational and informational purposes only.

## Content
- Code Examples (Some from lecture, most from Homework) are in the `src` folder. Java 8 is required to run some of the Code.
- Lecture Notes, which include quite a few Code Samples can be found in the `notes` folder.
- Exam Study Sheets, which are surprisingly similar to the actual exams in my experience, live in the `exam prep` folder.

## Plagiarism Notice

I am making my coursework available under the assumption that it will not be plagiarized. Doing so is in violation of the included license.

## License and Copyright Information

All rights reserved unless otherwise stated.

Text from course lessons are copyright-protected material owned by SDSU, and their inclusion in my coursework falls under fair use because context is required to understand my works.

My original content is under the [MIT license](https://opensource.org/licenses/MIT), and provided for educational purposes only with good faith and credit that it will not be plagiarized.

## Special Thanks

Adopted from my friend [Victor Lourng](https://github.com/LabLayers).